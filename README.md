# flate

A bit refreshed sources od Zlib.
A Massively Spiffy Yet Delicately Unobtrusive Compression Library
zlib was written by Jean-loup Gailly (compression) and Mark Adler (decompression)
https://www.zlib.net/
(especially https://www.zlib.net/zlib-1.2.11.tar.gz)
This repository contain conversion of piece of sources to C++ files.

Limitations:
- less configurable comparing to original
- no gzip, only deflation and inflation
- na classes (yet)

Requires https://github.com/catchorg/Catch2