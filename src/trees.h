#pragma once

#include "zcommon.h"
void gen_codes(ct_data* tree, int max_code, unsigned short* bl_count);
unsigned bi_reverse(unsigned code, int len);
void init_block(DeflateState* s);
void bi_windup(DeflateState* s);
void bi_flush(DeflateState* s);
int detect_data_type(DeflateState* s);
void compress_block(DeflateState* s, const ct_data* ltree, const ct_data* dtree);