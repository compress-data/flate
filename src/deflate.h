/* deflate.h -- internal compression state
 * Copyright (C) 1995-2016 Jean-loup Gailly
 * For conditions of distribution and use, see copyright notice in zlib.h
 */

/* WARNING: this file should *not* be used by applications. It is
   part of the implementation of the compression library and is
   subject to change. Applications should only use zlib.h.
 */

#ifndef DEFLATE_H
#define DEFLATE_H

#include "zcommon.h"
#include "zutil.h"

#define Freq fc.freq
#define Code fc.code
#define Dad  dl.dad
#define Len  dl.len

/* A Pos is an index in the character window. We use short instead of int to
 * save space in the various tables. IPos is used only for parameter passing.
 */

/* Output a byte on the stream.
 * IN assertion: there is enough room in pending_buf.
 */
#define put_byte(s, c) {s->pending_buf[s->pending++] = (uint8_t)(c);}


#define MIN_LOOKAHEAD (MAX_MATCH+MIN_MATCH+1)
/* Minimum amount of lookahead, except at the end of the input file.
 * See deflate.c for comments about the MIN_MATCH+1.
 */

#define MAX_DIST(s)  ((s)->w_size-MIN_LOOKAHEAD)
/* In order to simplify the code, particularly on 16 bit machines, match
 * distances are limited to MAX_DIST instead of WSIZE.
 */

#define WIN_INIT MAX_MATCH
/* Number of bytes after end of data in window to initialize in order to avoid
   memory checker errors from longest match routines */

        /* in trees.c */
void _tr_init(DeflateState *s);
int _tr_tally(DeflateState *s, unsigned dist, unsigned lc);
void _tr_flush_block(DeflateState *s, char *buf,
                        ulg stored_len, int last);
void _tr_flush_bits(DeflateState *s);
void _tr_align(DeflateState *s);
void _tr_stored_block(DeflateState *s, char *buf,
                        ulg stored_len, int last);

#define d_code(dist) \
   ((dist) < 256 ? _dist_code[dist] : _dist_code[256+((dist)>>7)])
/* Mapping from a distance to a distance code. dist is the distance - 1 and
 * must not have side effects. _dist_code[256] and _dist_code[257] are never
 * used.
 */

#ifndef ZLIB_DEBUG
/* Inline versions of _tr_tally for speed: */

#if defined(GEN_TREES_H) || !defined(STDC)
  extern uch _length_code[];
  extern uch _dist_code[];
#else
  extern const uch _length_code[];
  extern const uch _dist_code[];
#endif

# define _tr_tally_lit(s, c, flush) \
  { uch cc = (c); \
    s->d_buf[s->last_lit] = 0; \
    s->l_buf[s->last_lit++] = cc; \
    s->dyn_ltree[cc].Freq++; \
    flush = (s->last_lit == s->lit_bufsize-1); \
   }
# define _tr_tally_dist(s, distance, length, flush) \
  { uch len = (uch)(length); \
    ush dist = (ush)(distance); \
    s->d_buf[s->last_lit] = dist; \
    s->l_buf[s->last_lit++] = len; \
    dist--; \
    s->dyn_ltree[_length_code[len]+LITERALS+1].Freq++; \
    s->dyn_dtree[d_code(dist)].Freq++; \
    flush = (s->last_lit == s->lit_bufsize-1); \
  }
#else
# define _tr_tally_lit(s, c, flush) flush = _tr_tally(s, 0, c)
# define _tr_tally_dist(s, distance, length, flush) \
              flush = _tr_tally(s, distance, length)
#endif

void fill_window(DeflateState* s);
int deflateEnd(z_streamp strm);
int deflateInit2_(z_streamp strm, int  level, int  method, int  windowBits,
	int  memLevel, int  strategy, const char* version, int stream_size);
int deflateInit_(z_streamp strm, int level, const char* version, int stream_size);
int deflateReset(z_streamp strm);
int deflate(z_streamp strm, int flush);
int deflateParams(z_streamp strm, int level, int strategy);
int deflateSetDictionary(z_streamp strm, const uint8_t* dictionary, uInt  dictLength);

/* ===========================================================================
 *  Function prototypes.
 */
enum block_state {
	need_more,      /* block not completed, need more input or more output */
	block_done,     /* block flush performed */
	finish_started, /* finish started, need only more output at next deflate */
	finish_done     /* finish done, accept no more input or output */
};

void lm_init(DeflateState* s);
block_state deflate_stored(DeflateState* s, int flush);
block_state deflate_huff(DeflateState* s, int flush);


#endif /* DEFLATE_H */
