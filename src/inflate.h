/* zlib.h -- interface of the 'zlib' general purpose compression library
  version 1.2.11, January 15th, 2017

  Copyright (C) 1995-2017 Jean-loup Gailly and Mark Adler

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
	 claim that you wrote the original software. If you use this software
	 in a product, an acknowledgment in the product documentation would be
	 appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
	 misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Jean-loup Gailly        Mark Adler
  jloup@gzip.org          madler@alumni.caltech.edu


  The data format used by the zlib library is described by RFCs (Request for
  Comments) 1950 to 1952 in the files http://tools.ietf.org/html/rfc1950
  (zlib format), rfc1951 (deflate format) and rfc1952 (gzip format).
*/
#pragma once
#include "zcommon.h"
#include "inftrees.h"
#include "zutil.h"

#ifdef __STDC_VERSION__
#  ifndef STDC
#    define STDC
#  endif
#  if __STDC_VERSION__ >= 199901L
#    ifndef STDC99
#      define STDC99
#    endif
#  endif
#endif
#if !defined(STDC) && (defined(__STDC__) || defined(__cplusplus))
#  define STDC
#endif
#if !defined(STDC) && (defined(__GNUC__) || defined(__BORLANDC__))
#  define STDC
#endif
#if !defined(STDC) && (defined(MSDOS) || defined(WINDOWS) || defined(WIN32))
#  define STDC
#endif
#if !defined(STDC) && (defined(OS2) || defined(__HOS_AIX__))
#  define STDC
#endif

#if defined(__OS400__) && !defined(STDC)    /* iSeries (formerly AS/400). */
#  define STDC
#endif

#ifndef STDC
#  ifndef const /* cannot use !defined(STDC) && !defined(const) on Mac */
#    define const       /* note: need a more gentle solution here */
#  endif
#endif

#ifndef ZEXTERN
#  define ZEXTERN extern
#endif
#ifndef ZEXPORT
#  define ZEXPORT
#endif
#ifndef ZEXPORTVA
#  define ZEXPORTVA
#endif

/* ===========================================================================
 * Internal compression state.
 */

#define LENGTH_CODES 29
 /* number of length codes, not counting the special END_BLOCK code */

#define LITERALS  256
/* number of literal bytes 0..255 */

#define L_CODES (LITERALS+1+LENGTH_CODES)
/* number of Literal or Length codes, including the END_BLOCK code */

#define D_CODES   30
/* number of distance codes */

#define BL_CODES  19
/* number of codes used to transfer the bit lengths */

#define HEAP_SIZE (2*L_CODES+1)
/* maximum heap size */

#define MAX_BITS 15
/* All codes must not exceed MAX_BITS bits */

#define Buf_size 16
/* size of bit buffer in bi_buf */

#define INIT_STATE    42    /* zlib header -> BUSY_STATE */
#define EXTRA_STATE   69    /* gzip extra block -> NAME_STATE */
#define NAME_STATE    73    /* gzip file name -> COMMENT_STATE */
#define COMMENT_STATE 91    /* gzip comment -> HCRC_STATE */
#define HCRC_STATE   103    /* gzip header CRC -> BUSY_STATE */
#define BUSY_STATE   113    /* deflate -> FINISH_STATE */
#define FINISH_STATE 666    /* stream complete */
/* Stream status */

ZEXTERN int inflate OF((z_streamp strm, int flush));
/*
	inflate decompresses as much data as possible, and stops when the input
  buffer becomes empty or the output buffer becomes full.  It may introduce
  some output latency (reading input without producing any output) except when
  forced to flush.

  The detailed semantics are as follows.  inflate performs one or both of the
  following actions:

  - Decompress more input starting at next_in and update next_in and avail_in
	accordingly.  If not all input can be processed (because there is not
	enough room in the output buffer), then next_in and avail_in are updated
	accordingly, and processing will resume at this point for the next call of
	inflate().

  - Generate more output starting at next_out and update next_out and avail_out
	accordingly.  inflate() provides as much output as possible, until there is
	no more input data or no more space in the output buffer (see below about
	the flush parameter).

	Before the call of inflate(), the application should ensure that at least
  one of the actions is possible, by providing more input and/or consuming more
  output, and updating the next_* and avail_* values accordingly.  If the
  caller of inflate() does not provide both available input and available
  output space, it is possible that there will be no progress made.  The
  application can consume the uncompressed output when it wants, for example
  when the output buffer is full (avail_out == 0), or after each call of
  inflate().  If inflate returns Z_OK and with zero avail_out, it must be
  called again after making room in the output buffer because there might be
  more output pending.

	The flush parameter of inflate() can be Z_NO_FLUSH, Z_SYNC_FLUSH, Z_FINISH,
  Z_BLOCK, or Z_TREES.  Z_SYNC_FLUSH requests that inflate() flush as much
  output as possible to the output buffer.  Z_BLOCK requests that inflate()
  stop if and when it gets to the next deflate block boundary.  When decoding
  the zlib or gzip format, this will cause inflate() to return immediately
  after the header and before the first block.  When doing a raw inflate,
  inflate() will go ahead and process the first block, and will return when it
  gets to the end of that block, or when it runs out of data.

	The Z_BLOCK option assists in appending to or combining deflate streams.
  To assist in this, on return inflate() always sets strm->data_type to the
  number of unused bits in the last byte taken from strm->next_in, plus 64 if
  inflate() is currently decoding the last block in the deflate stream, plus
  128 if inflate() returned immediately after decoding an end-of-block code or
  decoding the complete header up to just before the first byte of the deflate
  stream.  The end-of-block will not be indicated until all of the uncompressed
  data from that block has been written to strm->next_out.  The number of
  unused bits may in general be greater than seven, except when bit 7 of
  data_type is set, in which case the number of unused bits will be less than
  eight.  data_type is set as noted here every time inflate() returns for all
  flush options, and so can be used to determine the amount of currently
  consumed input in bits.

	The Z_TREES option behaves as Z_BLOCK does, but it also returns when the
  end of each deflate block header is reached, before any actual data in that
  block is decoded.  This allows the caller to determine the length of the
  deflate block header for later use in random access within a deflate block.
  256 is added to the value of strm->data_type when inflate() returns
  immediately after reaching the end of the deflate block header.

	inflate() should normally be called until it returns Z_STREAM_END or an
  error.  However if all decompression is to be performed in a single step (a
  single call of inflate), the parameter flush should be set to Z_FINISH.  In
  this case all pending input is processed and all pending output is flushed;
  avail_out must be large enough to hold all of the uncompressed data for the
  operation to complete.  (The size of the uncompressed data may have been
  saved by the compressor for this purpose.)  The use of Z_FINISH is not
  required to perform an inflation in one step.  However it may be used to
  inform inflate that a faster approach can be used for the single inflate()
  call.  Z_FINISH also informs inflate to not maintain a sliding window if the
  stream completes, which reduces inflate's memory footprint.  If the stream
  does not complete, either because not all of the stream is provided or not
  enough output space is provided, then a sliding window will be allocated and
  inflate() can be called again to continue the operation as if Z_NO_FLUSH had
  been used.

	 In this implementation, inflate() always flushes as much output as
  possible to the output buffer, and always uses the faster approach on the
  first call.  So the effects of the flush parameter in this implementation are
  on the return value of inflate() as noted below, when inflate() returns early
  when Z_BLOCK or Z_TREES is used, and when inflate() avoids the allocation of
  memory for a sliding window when Z_FINISH is used.

	 If a preset dictionary is needed after this call (see inflateSetDictionary
  below), inflate sets strm->adler to the Adler-32 checksum of the dictionary
  chosen by the compressor and returns Z_NEED_DICT; otherwise it sets
  strm->adler to the Adler-32 checksum of all output produced so far (that is,
  total_out bytes) and returns Z_OK, Z_STREAM_END or an error code as described
  below.  At the end of the stream, inflate() checks that its computed Adler-32
  checksum is equal to that saved by the compressor and returns Z_STREAM_END
  only if the checksum is correct.

	inflate() can decompress and check either zlib-wrapped or gzip-wrapped
  deflate data.  The header type is detected automatically, if requested when
  initializing with inflateInit2().  Any information contained in the gzip
  header is not retained unless inflateGetHeader() is used.  When processing
  gzip-wrapped deflate data, strm->adler32 is set to the CRC-32 of the output
  produced so far.  The CRC-32 is checked against the gzip trailer, as is the
  uncompressed length, modulo 2^32.

	inflate() returns Z_OK if some progress has been made (more input processed
  or more output produced), Z_STREAM_END if the end of the compressed data has
  been reached and all uncompressed output has been produced, Z_NEED_DICT if a
  preset dictionary is needed at this point, Z_DATA_ERROR if the input data was
  corrupted (input stream not conforming to the zlib format or incorrect check
  value, in which case strm->msg points to a string with a more specific
  error), Z_STREAM_ERROR if the stream structure was inconsistent (for example
  next_in or next_out was Z_NULL, or the state was inadvertently written over
  by the application), Z_MEM_ERROR if there was not enough memory, Z_BUF_ERROR
  if no progress was possible or if there was not enough room in the output
  buffer when Z_FINISH is used.  Note that Z_BUF_ERROR is not fatal, and
  inflate() can be called again with more input and more output space to
  continue decompressing.  If Z_DATA_ERROR is returned, the application may
  then call inflateSync() to look for a good compression block if a partial
  recovery of the data is to be attempted.
*/


ZEXTERN int inflateEnd OF((z_streamp strm));
/*
	 All dynamically allocated data structures for this stream are freed.
   This function discards any unprocessed input and does not flush any pending
   output.

	 inflateEnd returns Z_OK if success, or Z_STREAM_ERROR if the stream state
   was inconsistent.
*/

typedef gz_header * gz_headerp;

/* Possible inflate modes between inflate() calls */
typedef enum {
	HEAD = 16180,   /* i: waiting for magic header */
	FLAGS,      /* i: waiting for method and flags (gzip) */
	TIME,       /* i: waiting for modification time (gzip) */
	OS,         /* i: waiting for extra flags and operating system (gzip) */
	EXLEN,      /* i: waiting for extra length (gzip) */
	EXTRA,      /* i: waiting for extra bytes (gzip) */
	NAME,       /* i: waiting for end of file name (gzip) */
	COMMENT,    /* i: waiting for end of comment (gzip) */
	HCRC,       /* i: waiting for header crc (gzip) */
	DICTID,     /* i: waiting for dictionary check value */
	DICT,       /* waiting for inflateSetDictionary() call */
	TYPE,       /* i: waiting for type bits, including last-flag bit */
	TYPEDO,     /* i: same, but skip check to exit inflate on new block */
	STORED,     /* i: waiting for stored size (length and complement) */
	COPY_,      /* i/o: same as COPY below, but only first time in */
	COPY,       /* i/o: waiting for input or output to copy stored block */
	TABLE,      /* i: waiting for dynamic block table lengths */
	LENLENS,    /* i: waiting for code length code lengths */
	CODELENS,   /* i: waiting for length/lit and distance code lengths */
	LEN_,       /* i: same as LEN below, but only first time in */
	LEN,        /* i: waiting for length/lit/eob code */
	LENEXT,     /* i: waiting for length extra bits */
	DIST,       /* i: waiting for distance code */
	DISTEXT,    /* i: waiting for distance extra bits */
	MATCH,      /* o: waiting for output space to copy string */
	LIT,        /* o: waiting for output space to write literal */
	CHECK,      /* i: waiting for 32-bit check value */
	LENGTH,     /* i: waiting for 32-bit length (gzip) */
	DONE,       /* finished check, done -- remain here until reset */
	BAD,        /* got a data error -- remain here until reset */
	MEM,        /* got an inflate() memory error -- remain here until reset */
	SYNC        /* looking for synchronization bytes to restart inflate() */
} inflate_mode;

/* State maintained between inflate() calls -- approximately 7K bytes, not
   including the allocated sliding window, which is up to 32K bytes. */
struct inflate_state {
	z_streamp strm;             /* pointer back to this zlib stream */
	inflate_mode mode;          /* current inflate mode */
	int last;                   /* true if processing last block */
	int wrap;                   /* bit 0 true for zlib, bit 1 true for gzip,
								   bit 2 true to validate check value */
	int havedict;               /* true if dictionary provided */
	int flags;                  /* gzip header method and flags (0 if zlib) */
	unsigned dmax;              /* zlib header max distance (INFLATE_STRICT) */
	unsigned long check;        /* protected copy of check value */
	unsigned long total;        /* protected copy of output count */
	gz_headerp head;            /* where to save gzip header information */
		/* sliding window */
	unsigned wbits;             /* log base 2 of requested window size */
	unsigned wsize;             /* window size or zero if not using window */
	unsigned whave;             /* valid bytes in the window */
	unsigned wnext;             /* window write index */
	unsigned char * window;  /* allocated sliding window, if needed */
		/* bit accumulator */
	unsigned long hold;         /* input bit accumulator */
	unsigned bits;              /* number of bits in "in" */
		/* for string and stored block copying */
	unsigned length;            /* literal or length of data to copy */
	unsigned offset;            /* distance back to copy string from */
		/* for table and code decoding */
	unsigned extra;             /* extra bits needed */
		/* fixed and dynamic code tables */
	code const * lencode;    /* starting table for length/literal codes */
	code const * distcode;   /* starting table for distance codes */
	unsigned lenbits;           /* index bits for lencode */
	unsigned distbits;          /* index bits for distcode */
		/* dynamic table building */
	unsigned ncode;             /* number of code length code lengths */
	unsigned nlen;              /* number of length code lengths */
	unsigned ndist;             /* number of distance code lengths */
	unsigned have;              /* number of code lengths in lens[] */
	code * next;             /* next available space in codes[] */
	unsigned short lens[320];   /* temporary storage for code lengths */
	unsigned short work[288];   /* work area for code table building */
	code codes[ENOUGH];         /* space for code tables */
	int sane;                   /* if false, allow invalid distance too far */
	int back;                   /* bits back of last unprocessed length/lit */
	unsigned was;               /* initial length of match */
};

void inflate_fast(z_streamp strm, unsigned start);
int inflate(z_streamp strm, int flush);
int inflateEnd(z_streamp strm);
int inflateInit_(z_streamp strm, const char* version, int stream_size);
int inflateSetDictionary(z_streamp strm, const uint8_t* dictionary, uInt dictLength);
int inflateSync(z_streamp strm);

