/* example.c -- usage example of the zlib compression library
 * Copyright (C) 1995-2006, 2011, 2016 Jean-loup Gailly
 * For conditions of distribution and use, see copyright notice in zlib.h
 */

#define CATCH_CONFIG_MAIN
#include <cstdint>
#include <catch2/catch.hpp>
#include "zutil.h"
#include "inflate.h"
#include "deflate.h"
#include "compress.h"
#include "uncompr.h"

static const char hello[] = "hello, hello!";

const uLong compressedBufSize = 40000;
uint8_t compressed[compressedBufSize];

const uLong uncompressedBufSize = 40000;
uint8_t uncompressed[uncompressedBufSize];
// "hello world" would be more standard, but the repeated "hello"
// stresses the compression code better, sorry...

static const char dictionary[] = "hello";
static uLong dictId;    // Adler32 value of the dictionary 

void CHECK_ERR(int err, const char* msg) {
	INFO(msg);
	REQUIRE(err == Z_OK);	
}

// ===========================================================================
// Test deflate() with small buffers

void test_deflate(uint8_t* compr, uLong comprLen)
{
	z_stream c_stream; // compression stream 
	int err;
	uLong len = (uLong)strlen(hello) + 1;

	err = deflateInit(&c_stream, Z_DEFAULT_COMPRESSION);
	CHECK_ERR(err, "deflateInit");

	c_stream.next_in = (const unsigned char*)hello;
	c_stream.next_out = compr;

	while (c_stream.total_in != len && c_stream.total_out < comprLen) {
		c_stream.avail_in = c_stream.avail_out = 1; // force small buffers 
		err = deflate(&c_stream, Z_NO_FLUSH);
		CHECK_ERR(err, "deflate");
	}
	// Finish the stream, still forcing small buffers: 
	for (;;) {
		c_stream.avail_out = 1;
		err = deflate(&c_stream, Z_FINISH);
		if (err == Z_STREAM_END) break;
		CHECK_ERR(err, "deflate");
	}

	err = deflateEnd(&c_stream);
	CHECK_ERR(err, "deflateEnd");
}

// ===========================================================================
// Test inflate() with small buffers
 
void test_inflate(uint8_t* compr, uLong comprLen, uint8_t* uncompr, uLong uncomprLen)
{
	int err;
	z_stream d_stream; // decompression stream 

	strcpy_s((char*)uncompr, uncomprLen, "garbage");

	d_stream.next_in = compr;
	d_stream.avail_in = 0;
	d_stream.next_out = uncompr;

	err = inflateInit(&d_stream);
	CHECK_ERR(err, "inflateInit");

	while (d_stream.total_out < uncomprLen && d_stream.total_in < comprLen) {
		d_stream.avail_in = d_stream.avail_out = 1; // force small buffers 
		err = inflate(&d_stream, Z_NO_FLUSH);
		if (err == Z_STREAM_END) break;
		CHECK_ERR(err, "inflate");
	}

	err = inflateEnd(&d_stream);
	CHECK_ERR(err, "inflateEnd");

	REQUIRE(strcmp((char*)uncompr, hello) == 0);
}

// ===========================================================================
// Test deflate() with large buffers and dynamic change of compression level
 
void test_large_deflate(uint8_t* compr, uLong comprLen, uint8_t *uncompr, uLong uncomprLen)
{
	z_stream c_stream; // compression stream 
	int err;

	err = deflateInit(&c_stream, Z_BEST_SPEED);
	CHECK_ERR(err, "deflateInit");

	c_stream.next_out = compr;
	c_stream.avail_out = (uInt)comprLen;

	// At this point, uncompr is still mostly zeroes, so it should compress
	// very well:
	 
	c_stream.next_in = uncompr;
	c_stream.avail_in = (uInt)uncomprLen;
	err = deflate(&c_stream, Z_NO_FLUSH);
	CHECK_ERR(err, "deflate");
	INFO("deflate not greedy");
	REQUIRE(c_stream.avail_in == 0);
	
	// Feed in already compressed data and switch to no compression: 
	deflateParams(&c_stream, Z_NO_COMPRESSION, Z_DEFAULT_STRATEGY);
	c_stream.next_in = compr;
	c_stream.avail_in = (uInt)comprLen / 2;
	err = deflate(&c_stream, Z_NO_FLUSH);
	CHECK_ERR(err, "deflate");

	// Switch back to compressing mode: 
	deflateParams(&c_stream, Z_BEST_COMPRESSION, Z_FILTERED);
	c_stream.next_in = uncompr;
	c_stream.avail_in = (uInt)uncomprLen;
	err = deflate(&c_stream, Z_NO_FLUSH);
	CHECK_ERR(err, "deflate");

	err = deflate(&c_stream, Z_FINISH);
	INFO("deflate should report Z_STREAM_END");
	REQUIRE(err == Z_STREAM_END);
	err = deflateEnd(&c_stream);
	CHECK_ERR(err, "deflateEnd");
}

// ===========================================================================
// Test inflate() with large buffers
 
void test_large_inflate(uint8_t* compr, uLong comprLen, uint8_t* uncompr, uLong uncomprLen)
{
	int err;
	z_stream d_stream; // decompression stream 

	strcpy_s((char*)uncompr, uncomprLen, "garbage");

	d_stream.next_in = compr;
	d_stream.avail_in = (uInt)comprLen;

	err = inflateInit(&d_stream);
	CHECK_ERR(err, "inflateInit");

	for (;;) {
		d_stream.next_out = uncompr;            // discard the output 
		d_stream.avail_out = (uInt)uncomprLen;
		err = inflate(&d_stream, Z_NO_FLUSH);
		if (err == Z_STREAM_END) break;
		CHECK_ERR(err, "large inflate");
	}

	err = inflateEnd(&d_stream);
	CHECK_ERR(err, "inflateEnd");

	INFO("bad large inflate");
	REQUIRE(d_stream.total_out == 2 * uncomprLen + comprLen / 2);	
}

// ===========================================================================
// Test deflate() with full flush

void test_flush(uint8_t* compr, uLong* comprLen)
{
	z_stream c_stream; // compression stream 
	int err;
	uInt len = (uInt)strlen(hello) + 1;

	err = deflateInit(&c_stream, Z_DEFAULT_COMPRESSION);
	CHECK_ERR(err, "deflateInit");

	c_stream.next_in = (const unsigned char*)hello;
	c_stream.next_out = compr;
	c_stream.avail_in = 3;
	c_stream.avail_out = (uInt)* comprLen;
	err = deflate(&c_stream, Z_FULL_FLUSH);
	CHECK_ERR(err, "deflate");

	compr[3]++; // force an error in first compressed block 
	c_stream.avail_in = len - 3;

	err = deflate(&c_stream, Z_FINISH);
	if (err != Z_STREAM_END) {
		CHECK_ERR(err, "deflate");
	}
	err = deflateEnd(&c_stream);
	CHECK_ERR(err, "deflateEnd");

	*comprLen = c_stream.total_out;
}

// ===========================================================================
// Test inflateSync()
 
void test_sync(uint8_t* compr, uLong comprLen, uint8_t* uncompr, uLong uncomprLen)
{
	int err;
	z_stream d_stream; // decompression stream

	strcpy_s((char*)uncompr, uncomprLen, "garbage");

	d_stream.next_in = compr;
	d_stream.avail_in = 2; // just read the zlib header 

	err = inflateInit(&d_stream);
	CHECK_ERR(err, "inflateInit");

	d_stream.next_out = uncompr;
	d_stream.avail_out = (uInt)uncomprLen;

	err = inflate(&d_stream, Z_NO_FLUSH);
	CHECK_ERR(err, "inflate");

	d_stream.avail_in = (uInt)comprLen - 2;   // read all compressed data 
	err = inflateSync(&d_stream);           // but skip the damaged part 
	CHECK_ERR(err, "inflateSync");

	err = inflate(&d_stream, Z_FINISH);
	INFO("inflate should report DATA_ERROR");
	REQUIRE(err == Z_DATA_ERROR);// Because of incorrect adler32 
	
	err = inflateEnd(&d_stream);
	CHECK_ERR(err, "inflateEnd");

	printf("after inflateSync(): hel%s\n", (char*)uncompr);
}

// ===========================================================================
// Test deflate() with preset dictionary

void test_dict_deflate(uint8_t* compr, uLong comprLen)
{
	z_stream c_stream; // compression stream 
	int err;

	err = deflateInit(&c_stream, Z_BEST_COMPRESSION);
	CHECK_ERR(err, "deflateInit");

	err = deflateSetDictionary(&c_stream,
		(const uint8_t*)dictionary, (int)sizeof(dictionary));
	CHECK_ERR(err, "deflateSetDictionary");

	dictId = c_stream.adler;
	c_stream.next_out = compr;
	c_stream.avail_out = (uInt)comprLen;

	c_stream.next_in = (const unsigned char*)hello;
	c_stream.avail_in = (uInt)strlen(hello) + 1;

	err = deflate(&c_stream, Z_FINISH);
	INFO("deflate should report Z_STREAM_END");
	REQUIRE(err == Z_STREAM_END);
	err = deflateEnd(&c_stream);
	CHECK_ERR(err, "deflateEnd");
}

//  ===========================================================================
// Test inflate() with a preset dictionary
void test_dict_inflate(uint8_t* compr, uLong comprLen, uint8_t* uncompr, uLong uncomprLen)
{
	int err;
	z_stream d_stream; // decompression stream

	strcpy_s((char*)uncompr, uncomprLen, "garbage");

	d_stream.next_in = compr;
	d_stream.avail_in = (uInt)comprLen;

	err = inflateInit(&d_stream);
	CHECK_ERR(err, "inflateInit");

	d_stream.next_out = uncompr;
	d_stream.avail_out = (uInt)uncomprLen;

	for (;;) {
		err = inflate(&d_stream, Z_NO_FLUSH);
		if (err == Z_STREAM_END) break;
		if (err == Z_NEED_DICT) {
			INFO("unexpected dictionary");
			REQUIRE(d_stream.adler == dictId);
			err = inflateSetDictionary(&d_stream, (const uint8_t*)dictionary,
				(int)sizeof(dictionary));
		}
		CHECK_ERR(err, "inflate with dict");
	}

	err = inflateEnd(&d_stream);
	CHECK_ERR(err, "inflateEnd");

	INFO("bad inflate with dict");
	REQUIRE(strcmp((char*)uncompr, hello)==0);	
}

void test_compress(uint8_t* compr, uLong comprLen, uint8_t* uncompr, uLong uncomprLen)
{
	int err;
	uLong len = (uLong)strlen(hello) + 1;

	err = compress(compr, &comprLen, (const uint8_t*)hello, len);
	CHECK_ERR(err, "compress");

	strcpy_s((char*)uncompr, uncomprLen, "garbage");

	err = uncompress(uncompr, &uncomprLen, compr, comprLen);
	CHECK_ERR(err, "uncompress");

	INFO("bad uncompress");
	REQUIRE(strcmp((char*)uncompr, hello) == 0);
}


TEST_CASE("Test deflate and inflate") {
	test_deflate(compressed, compressedBufSize);
	test_inflate(compressed, compressedBufSize, uncompressed, uncompressedBufSize);
}

TEST_CASE("Test deflate and inflate large") {
	test_large_deflate(compressed, compressedBufSize, uncompressed, uncompressedBufSize);
	test_large_inflate(compressed, compressedBufSize, uncompressed, uncompressedBufSize);
}

TEST_CASE("Test dict") {
	test_dict_deflate(compressed, compressedBufSize);
	test_dict_inflate(compressed, compressedBufSize, uncompressed, uncompressedBufSize);
}

TEST_CASE("Test flush and sync") {
	uLong comprLen = compressedBufSize;
	test_flush(compressed, &comprLen);
	test_sync(compressed, comprLen, uncompressed, uncompressedBufSize);
}


TEST_CASE("Test version") {
	static const char* myVersion = ZLIB_VERSION;
	REQUIRE(zlibVersion()[0] == myVersion[0]);
	REQUIRE(strcmp(zlibVersion(), ZLIB_VERSION) == 0);
}

TEST_CASE("Test compress") {
	test_compress(compressed, compressedBufSize, uncompressed, uncompressedBufSize);
}
