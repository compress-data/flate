#pragma once

#ifndef OF /* function prototypes */
#  ifdef STDC
#    define OF(args)  args
#  else
#    define OF(args)  ()
#  endif
#endif

#  include <limits.h>
#  if (UINT_MAX == 0xffffffffUL)
#    define Z_U4 unsigned
#  elif (ULONG_MAX == 0xffffffffUL)
#    define Z_U4 unsigned long
#  elif (USHRT_MAX == 0xffffffffUL)
#    define Z_U4 unsigned short
#  endif

typedef Z_U4 z_crc_t;
typedef unsigned int   uInt;  /* 16 bits or more */
typedef unsigned long  uLong; /* 32 bits or more */

/* define NO_GZIP when compiling if you want to disable gzip header and
   trailer creation by deflate().  NO_GZIP would be used to avoid linking in
   the crc code when it is not needed.  For shared libraries, gzip encoding
   should be left enabled. */
#ifndef NO_GZIP
#  define GZIP
#endif

/* Maximum value for windowBits in deflateInit2 and inflateInit2.
* WARNING: reducing MAX_WBITS makes minigzip unable to extract .gz files
* created by gzip. (Files created by minigzip can still be extracted by
* gzip.)
*/
const int MAX_WBITS = 15; /* 32K LZ77 window */

/* Maximum value for memLevel in deflateInit2 */
#ifndef MAX_MEM_LEVEL
#  ifdef MAXSEG_64K
const int MAX_MEM_LEVEL = 8;
#  else
const int MAX_MEM_LEVEL = 9;
#  endif
#endif



typedef struct z_stream_s {
	const uint8_t* next_in;     /* next input byte */
	uInt     avail_in;  /* number of bytes available at next_in */
	uLong    total_in;  /* total number of input bytes read so far */

	uint8_t* next_out; /* next output byte will go here */
	uInt     avail_out; /* remaining free space at next_out */
	uLong    total_out; /* total number of bytes output so far */

	const char* msg;  /* last error message, NULL if no error */
	struct DeflateState* state; /* not visible by applications */

	int     data_type;  /* best guess about the data type: binary or text
						   for deflate, or the decoding state for inflate */
	uLong   adler;      /* Adler-32 or CRC-32 value of the uncompressed data */
	uLong   reserved;   /* reserved for future use */
} z_stream;

/*
	 gzip header information passed to and from zlib routines.  See RFC 1952
  for more details on the meanings of these fields.
*/
typedef struct gz_header_s {
	int     text;       /* true if compressed data believed to be text */
	uLong   time;       /* modification time */
	int     xflags;     /* extra flags (not used when writing a gzip file) */
	int     os;         /* operating system */
	uint8_t* extra;     /* pointer to extra field or Z_NULL if none */
	uInt    extra_len;  /* extra field length (valid if extra != Z_NULL) */
	uInt    extra_max;  /* space at extra (only when reading header) */
	uint8_t* name;      /* pointer to zero-terminated file name or Z_NULL */
	uInt    name_max;   /* space at name (only when reading header) */
	uint8_t* comment;   /* pointer to zero-terminated comment or Z_NULL */
	uInt    comm_max;   /* space at comment (only when reading header) */
	int     hcrc;       /* true if there was or will be a header crc */
	int     done;       /* true when done reading gzip header (not used
						   when writing a gzip file) */
} gz_header;

typedef gz_header* gz_headerp;
typedef z_stream* z_streamp;

#define Z_OK            0
#define Z_STREAM_END    1
#define Z_NEED_DICT     2
#define Z_ERRNO        (-1)
#define Z_STREAM_ERROR (-2)
#define Z_DATA_ERROR   (-3)
#define Z_MEM_ERROR    (-4)
#define Z_BUF_ERROR    (-5)
#define Z_VERSION_ERROR (-6)
/* Return codes for the compression/decompression functions. Negative values
 * are errors, positive values are used for special but normal events.
 */

#define Z_DEFLATED   8
 /* The deflate compression method (the only one supported in this version) */

#define Z_NO_FLUSH      0
#define Z_PARTIAL_FLUSH 1
#define Z_SYNC_FLUSH    2
#define Z_FULL_FLUSH    3
#define Z_FINISH        4
#define Z_BLOCK         5
#define Z_TREES         6
/* Allowed flush values; see deflate() and inflate() below for details */

#define Z_FILTERED            1
#define Z_HUFFMAN_ONLY        2
#define Z_RLE                 3
#define Z_FIXED               4
#define Z_DEFAULT_STRATEGY    0
/* compression strategy; see deflateInit2() below for details */

#define Z_NO_COMPRESSION         0
#define Z_BEST_SPEED             1
#define Z_BEST_COMPRESSION       9
#define Z_DEFAULT_COMPRESSION  (-1)
/* compression levels */

#define Z_BINARY   0
#define Z_TEXT     1
#define Z_ASCII    Z_TEXT   /* for compatibility with 1.2.2 and earlier */
#define Z_UNKNOWN  2
/* Possible values of the data_type field for deflate() */

/* ===========================================================================
 * Internal compression state.
 */

#define LENGTH_CODES 29
 /* number of length codes, not counting the special END_BLOCK code */

#define LITERALS  256
/* number of literal bytes 0..255 */

#define L_CODES (LITERALS+1+LENGTH_CODES)
/* number of Literal or Length codes, including the END_BLOCK code */

#define D_CODES   30
/* number of distance codes */

#define BL_CODES  19
/* number of codes used to transfer the bit lengths */

#define HEAP_SIZE (2*L_CODES+1)
/* maximum heap size */

#define MAX_BITS 15
/* All codes must not exceed MAX_BITS bits */

#define Buf_size 16
/* size of bit buffer in bi_buf */

#define INIT_STATE    42    /* zlib header -> BUSY_STATE */
#ifdef GZIP
#  define GZIP_STATE  57    /* gzip header -> BUSY_STATE | EXTRA_STATE */
#endif
#define EXTRA_STATE   69    /* gzip extra block -> NAME_STATE */
#define NAME_STATE    73    /* gzip file name -> COMMENT_STATE */
#define COMMENT_STATE 91    /* gzip comment -> HCRC_STATE */
#define HCRC_STATE   103    /* gzip header CRC -> BUSY_STATE */
#define BUSY_STATE   113    /* deflate -> FINISH_STATE */
#define FINISH_STATE 666    /* stream complete */
/* Stream status */

typedef unsigned short Pos;
typedef Pos Posf;
typedef unsigned short ush;
typedef unsigned IPos;
typedef unsigned long ulg;
typedef unsigned char uch, uchf;

/* Data structure describing a single value and its code string. */
typedef struct ct_data_s {
	union {
		ush  freq;       /* frequency count */
		ush  code;       /* bit string */
	} fc;
	union {
		ush  dad;        /* father node in Huffman tree */
		ush  len;        /* length of bit string */
	} dl;
} ct_data;

typedef struct static_tree_desc_s  static_tree_desc;

typedef struct tree_desc_s {
	ct_data* dyn_tree;           /* the dynamic tree */
	int     max_code;            /* largest code with non zero frequency */
	const static_tree_desc* stat_desc;  /* the corresponding static tree */
} tree_desc;
#ifdef Z_PREFIX_SET
#  define z_deflateInit(strm, level) \
          deflateInit_((strm), (level), ZLIB_VERSION, (int)sizeof(z_stream))
#  define z_inflateInit(strm) \
          inflateInit_((strm), ZLIB_VERSION, (int)sizeof(z_stream))
#  define z_deflateInit2(strm, level, method, windowBits, memLevel, strategy) \
          deflateInit2_((strm),(level),(method),(windowBits),(memLevel),\
                        (strategy), ZLIB_VERSION, (int)sizeof(z_stream))
#  define z_inflateInit2(strm, windowBits) \
          inflateInit2_((strm), (windowBits), ZLIB_VERSION, \
                        (int)sizeof(z_stream))
#  define z_inflateBackInit(strm, windowBits, window) \
          inflateBackInit_((strm), (windowBits), (window), \
                           ZLIB_VERSION, (int)sizeof(z_stream))
#else
#  define deflateInit(strm, level) \
          deflateInit_((strm), (level), ZLIB_VERSION, (int)sizeof(z_stream))
#  define inflateInit(strm) \
          inflateInit_((strm), ZLIB_VERSION, (int)sizeof(z_stream))
#  define deflateInit2(strm, level, method, windowBits, memLevel, strategy) \
          deflateInit2_((strm),(level),(method),(windowBits),(memLevel),\
                        (strategy), ZLIB_VERSION, (int)sizeof(z_stream))
#  define inflateInit2(strm, windowBits) \
          inflateInit2_((strm), (windowBits), ZLIB_VERSION, \
                        (int)sizeof(z_stream))
#  define inflateBackInit(strm, windowBits, window) \
          inflateBackInit_((strm), (windowBits), (window), \
                           ZLIB_VERSION, (int)sizeof(z_stream))
#endif
