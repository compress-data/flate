#pragma once
#include "deflate.h"
int compress(uint8_t* dest, uLong* destLen, const uint8_t* source, uLong sourceLen);
