#pragma once
/* Definitions for doing the crc four data bytes at a time. */
#if !defined(NOBYFOUR) && defined(Z_U4)
#  define BYFOUR
#endif
#ifdef BYFOUR
#  define TBLS 8
#else
#  define TBLS 1
#endif /* BYFOUR */

unsigned long crc32_little(unsigned long crc, const unsigned char* buf, size_t len);
unsigned long crc32_big(unsigned long crc, const unsigned char* buf, size_t len);

/* functions for crc concatenation */
unsigned long gf2_matrix_times(unsigned long* mat,
	unsigned long vec);
void gf2_matrix_square(unsigned long* square, unsigned long* mat);
unsigned long crc32_combine_(unsigned long crc1, unsigned long crc2, size_t len2);
unsigned long crc32(unsigned long crc, const unsigned char* buf, uInt len);

#ifdef DYNAMIC_CRC_TABLE
#else /* !DYNAMIC_CRC_TABLE */
/* ========================================================================
 * Tables of CRC-32s of all single-byte values, made by make_crc_table().
 */
#include "crc32tab.h"
#endif /* DYNAMIC_CRC_TABLE */

